﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kavenegar;
using shop.Models;

namespace shop.Classes
{
    public class SmsSender
    {
        public void Send(string To, string Body)
        {
            DatabaseContext db = new DatabaseContext();
            var setting = db.Settings.FirstOrDefault();
            var sender = setting.SmsSender;
            var receptor = To;
            var message = Body;
            var api = new KavenegarApi(setting.SmsApi);
            api.Send(sender, receptor, message);
        }
    }
}