﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.UI;

namespace shop.Models
{
    public class ProductFeild
    {
        [Key]
        public int Id { get; set; }

        public int ProductId { get; set; }

        public int FeildId { get; set; }

         [Display(Name ="مقدار مشخصه")]
         [Required(ErrorMessage ="مقدار برای {0} وارد نمایید")]
         [MaxLength(50, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
        public string FeldValue { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
        [ForeignKey("FeildId")]
        public virtual Feild Feild { get; set; }
    }
}