﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace shop.Models
{
    public class Setting
    {
         [Key]
        public int Id { get; set; }

        [Display(Name ="نام فروشگاه")] 
        [MaxLength(100, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        public string ShopName { get; set; }

        [Display(Name ="توضیحات فروشگاه")]
        [DataType(DataType.MultilineText)]
        public string ShopDes { get; set; }


        [Display(Name = "کلمات کلیدی")]
        [DataType(DataType.MultilineText)]
        public string ShopKey { get; set; }


        [Display(Name ="شماره تماس فروشگاه")]
        [MaxLength(20, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        public string ShopTel { get; set; }


        [Display(Name ="آدرس فروشگاه")]
         [MaxLength(200, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        public string ShopAddress { get; set; }


        [Display(Name ="شماره ارسال")]
        [MaxLength(20, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        public string SmsSender { get; set; }


        [Display(Name ="کلید پیامک")]
        public string SmsApi { get; set; }


        [Display(Name ="پیامک پس از فاکتور")]
        public bool FactorIsSend { get; set; }


        [Display(Name ="پیامک پس از پرداخت")]
        public bool PayIsSend { get; set; }

    }
}