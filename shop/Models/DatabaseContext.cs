﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace shop.Models
{
    public class DatabaseContext : DbContext
    {
        static DatabaseContext (){
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DatabaseContext, Migrations.Configuration>());
        }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Social> Socials { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Slider> Sliders { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Product> products { get; set; }
        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<Feild> Feilds { get; set; }
        public DbSet<ProductFeild> ProductFeilds { get; set; }
    }
}