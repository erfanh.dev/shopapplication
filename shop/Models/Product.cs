﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
namespace shop.Models
{
    public class Product
    {
        [Key]
        public int id { get; set; }
        
        public int BrandId { get; set; }
        public int GroupId { get; set; }

         [Display(Name ="نام محصول")]
         [Required(ErrorMessage ="مقدار برای {0} وارد نمایید")]
         [MaxLength(50, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
        public string Name { get; set; }

         [Display(Name ="قیمت واحد")]
        public int price { get; set; }

         [Display(Name ="تصویر شاخص")]
         [MaxLength(100, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
        public string Img { get; set; }

         [Display(Name ="توضیحات محصول ")]
         [DataType(DataType.MultilineText)]
         [AllowHtml]
        public string Des { get; set; }

         [Display(Name ="تعداد بازدید ")]
        public int seen { get; set; }

         [Display(Name ="موجودی ")]
        public int Mojoodi { get; set; }

         [Display(Name ="عدم نمایش ")]
        public bool NotShow { get; set; }

        [ForeignKey("GroupId")]
        public virtual Group Group { get; set; }

        [ForeignKey("BrandId")]
        public virtual Brand Brand { get; set; }

        public virtual ICollection<Gallery> Gallerys { get; set; }

        public virtual ICollection<ProductFeild> ProductFeilds { get; set; }

    }
}