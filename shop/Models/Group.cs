﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace shop.Models
{
    public class Group
    {
        [Key]
        public int Id { get; set; }

        [Display(Name ="نام گروه")]
        [Required(ErrorMessage ="مقدار برای {0} وارد نمایید")]
        [MaxLength(40, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
        public string GroupName { get; set; }

        [Display(Name ="ترتیب نمایش")]
        public int GroupOrder { get; set; }

        [Display(Name ="عدم نمایش")]
        public bool GroupNotShow { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}