﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace shop.Models
{
    public class Role
    {
        [Key]
        public int Id { get; set; }

        [Display(Name ="نام نقش")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(20, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        public string Rolename { get; set; }

        [Display(Name ="عنوان نقش")]
        [MaxLength(20, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        public string Roletitle { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
