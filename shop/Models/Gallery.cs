﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace shop.Models
{
    public class Gallery
    {
        [Key]
        public int Id { get; set; }
        public int ProductId { get; set; }

         [Display(Name ="تصویر گالری")]
               [MaxLength(100, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
        public string Img { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
    }
}