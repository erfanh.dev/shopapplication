﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace shop.Models
{
    public class Brand
    {
        [Key]
        public int Id { get; set; }

        [Display(Name ="نام برند")]
        [Required(ErrorMessage ="مقدار برای {0} وارد نمایید")]
        [MaxLength(50, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
        public string BrandName { get; set; }

        [Display(Name ="انتخاب تصویر")]
        [MaxLength(100, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
        public string BrandImg { get; set; }

         [Display(Name ="ترتیب نمایش")]
        public int BrandOrder { get; set; }

         [Display(Name ="عدم نمایش")]
        public bool BrandNotShow { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}