﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace shop.Models
{
    public class Menu
    {
        [Key]
        public int Id { get; set; }

        [Display(Name ="نام منو")]
        [Required(ErrorMessage ="مقدار برای {0} وارد نمایید")]
        [MaxLength(50, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
        public string NameMenu { get; set; }

        [Display(Name = "توضیحات منو")]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string MenuDes { get; set; }

        [Display(Name = "ترتیب نمایش منو")]
        public int MenuOrder { get; set; }

        [Display(Name = "عدم نمایش منو")]
        public bool MenuNotshow { get; set; }

    }
}