﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace shop.Models
{
    public class Feild
    {
        [Key]
        public int Id { get; set; }

         [Display(Name ="نام مشخصه")]
         [Required(ErrorMessage ="مقدار برای {0} وارد نمایید")]
         [MaxLength(50, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
        public string Name { get; set; }

        public virtual ICollection<ProductFeild> ProductFeilds { get; set; }

    }
}