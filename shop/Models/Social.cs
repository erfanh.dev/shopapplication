﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace shop.Models
{
    public class Social
    {
         [Key]
        public int Id { get; set; }


        [Display(Name ="نام شبکه")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(50, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        public string SocialName { get; set; }


        [Display(Name = "لینک شبکه")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(100, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        public string SocialLink { get; set; }


        [Display(Name = "آیکون شبکه")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(50, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        public string socialIcon { get; set; }

        [Display(Name ="عدم نمایش")]
        public bool SocialNotShow { get; set; }

        [Display(Name ="ترتیب نمایش")]
        public string SocialOrder { get; set; }
    }
}