﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace shop.Models
{
    public class RegisterViewModel
    {
        [Display(Name = "شماره موبایل")]
        [MaxLength(11, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        public string UserMobile { get; set; }
        [Display(Name = "رمز عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(50, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        public string UserPassword { get; set; }

        [Display(Name = "تکرار رمز عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(50, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        [Compare("UserPassword", ErrorMessage ="رمز انتخابی بایکدیگر همخوانی ندارد")]
        public string ComfirmUserPassword { get; set; }
    }
    public class LoginViewModel
    {
        [Display(Name = "شماره موبایل")]
        [MaxLength(11, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        public string UserMobile { get; set; }
        [Display(Name = "رمز عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(50, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        public string UserPassword { get; set; }
    }
    public class ActivateViewModel
    {
        [Display(Name = "کدفعالسازی")]
        [MaxLength(6, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        public string UserCode { get; set; }
    }
    public class CheckMobileViewModel
    {
        [Display(Name = "شماره موبایل")]
        [MaxLength(11, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        public string UserMobile { get; set; }
    }
    public class ForgetPasswordViewModel
    {
        [Display(Name = "کد تایید")]
        [MaxLength(6, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        public string UserCode { get; set; }

         [Display(Name = "رمز عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(50, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        public string UserPassword { get; set; }

        [Display(Name = "تکرار رمز عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(50, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        [Compare("UserPassword", ErrorMessage ="رمز انتخابی بایکدیگر همخوانی ندارد")]
        public string ComfirmUserPassword { get; set; }
    }
    public class ChangePasswordViewModel
    {

        [Display(Name = "رمز عبور فعلی")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(50, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        public string UserOldPassword { get; set; }

        [Display(Name = "رمز عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(50, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        public string UserPassword { get; set; }

        [Display(Name = "تکرار رمز عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(50, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        [Compare("UserPassword", ErrorMessage = "رمز انتخابی بایکدیگر همخوانی ندارد")]
        public string ComfirmUserPassword { get; set; }
    }
}