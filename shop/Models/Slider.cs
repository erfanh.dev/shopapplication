﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace shop.Models
{
    public class Slider
    {
 
        [Key]
        public int Id { get; set; }

         [Display(Name ="عنوان اسلاید")]
         [MaxLength(50, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
        public string SliderTitle { get; set; }

         [Display(Name ="عکس اسلاید")]
         [MaxLength(50, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
        public string SliderImage { get; set; }

         [Display(Name ="لینک اسلاید")]
        public string SlidetLink { get; set; }

         [Display(Name ="عدم نمایش")]
       public bool SliderNotShow { get; set; }
    }
}