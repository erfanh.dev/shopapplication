﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace shop.Models
{
    public class Address
    {
         [Key]
        public int Id { get; set; }
        public int UserId { get; set; }

        [Display(Name ="آدرس")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [DataType(DataType.MultilineText)]
        public string AddressText { get; set; }

        [Display(Name ="استان")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(40, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        public string AddressState { get; set; }

        [Display(Name ="شهرستان")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(40, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        public string AddressCity { get; set; }

        [Display(Name ="کدپستی")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(10, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        public string AddressPostalCode { get; set; }
         [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}