﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace shop.Models
{
    public class User
    {
         [Key]
        public int Id { get; set; }
        public int RoleId { get; set; }

        [Display(Name ="شماره موبایل")]
        [MaxLength(11, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")] 
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        public string UserMobile { get; set; }
        [Display(Name ="رمز عبور")] 
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(50, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        public string UserPassword { get; set; }

        [Display(Name ="کدفعالسازی")]
         [MaxLength(6, ErrorMessage = "مقدار نباید بیشتر از{1} کاراکتر باشد")]
        public string UserCode { get; set; }

        [Display(Name ="فعال ")]
        public bool IsActive { get; set; }
        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
    }
}