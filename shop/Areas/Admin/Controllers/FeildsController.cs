﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using shop.Models;

namespace shop.Areas.Admin.Controllers
{
    public class FeildsController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: Admin/Feilds
        public ActionResult Index()
        {
            return View(db.Feilds.ToList());
        }

       
        // GET: Admin/Feilds/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Feilds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] Feild feild)
        {
            if (ModelState.IsValid)
            {
                db.Feilds.Add(feild);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(feild);
        }

        // GET: Admin/Feilds/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Feild feild = db.Feilds.Find(id);
            if (feild == null)
            {
                return HttpNotFound();
            }
            return View(feild);
        }

        // POST: Admin/Feilds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] Feild feild)
        {
            if (ModelState.IsValid)
            {
                db.Entry(feild).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(feild);
        }

        // GET: Admin/Feilds/Delete/5
        public ActionResult Delete(int? id)
        {
            Feild feild = db.Feilds.Find(id);
            db.Feilds.Remove(feild);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
