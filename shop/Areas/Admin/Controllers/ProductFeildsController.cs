﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using shop.Models;

namespace shop.Areas.Admin.Controllers
{
    public class ProductFeildsController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: Admin/ProductFeilds
        public ActionResult Index(int? id)
        {
            var productFeilds = db.ProductFeilds.Where(p=> p.ProductId == id).Include(p => p.Feild).Include(p => p.Product);
            ViewBag.myid = id;
            return View(productFeilds.ToList());
        }

       
        // GET: Admin/ProductFeilds/Create
        public ActionResult Create(int? id)
        {
            ViewBag.FeildId = new SelectList(db.Feilds, "Id", "Name");
            ViewBag.ProductId = new SelectList(db.products.Where(p=> p.id == id).ToList(), "id", "Name");
            return PartialView();
        }

        // POST: Admin/ProductFeilds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ProductId,FeildId,FeldValue")] ProductFeild productFeild, int? id)
        {
            if (ModelState.IsValid)
            {
                db.ProductFeilds.Add(productFeild);
                db.SaveChanges();
                return Redirect("/Admin/ProductFeilds/Index/" + id);
            }

            ViewBag.FeildId = new SelectList(db.Feilds, "Id", "Name", productFeild.FeildId);
            ViewBag.ProductId = new SelectList(db.products.Where(p => p.id == id).ToList(), "id", "Name", productFeild.ProductId);
            return PartialView(productFeild);
        }

      

        // GET: Admin/ProductFeilds/Delete/5
        public ActionResult Delete(int? id)
        {
            ProductFeild productFeild = db.ProductFeilds.Find(id);
            db.ProductFeilds.Remove(productFeild);
            db.SaveChanges();
            return Redirect("/Admin/ProductFeilds/Index/" + id);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
