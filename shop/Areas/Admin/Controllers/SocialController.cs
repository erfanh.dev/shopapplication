﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using shop.Models;

namespace shop.Areas.Admin.Controllers
{
    public class SocialController : Controller
    {
        DatabaseContext db = new DatabaseContext();
        // GET: Admin/Social
        public ActionResult Index()
        {
            return View(db.Socials.ToList());
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Social social)
        {
            if (ModelState.IsValid)
            {
                Social so = new Social() {

                    SocialName= social.SocialName,
                    socialIcon= social.socialIcon,
                    SocialLink= social.SocialLink,
                    SocialOrder= social.SocialOrder,
                    SocialNotShow= social.SocialNotShow
                
                };
                db.Socials.Add(so);
                db.SaveChanges();
                return RedirectToAction("Index");

            }
            return View(social);
        }
        public ActionResult Edit(int? id)
        {
            var social = db.Socials.Find(id);
            return View(social);
        }
        [HttpPost]
        public ActionResult Edit(int? id, Social social)
        {
            var so = db.Socials.Find(id);
            so.SocialName = social.SocialName;
            so.SocialLink = social.SocialLink;
            so.socialIcon = social.socialIcon;
            so.SocialOrder = social.SocialOrder;
            so.SocialNotShow = social.SocialNotShow;
            db.SaveChanges();
            return View(social);
        }
        public ActionResult Delete(int? id)
        {
            var social = db.Socials.Find(id);
            db.Socials.Remove(social);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}