﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using shop.Models;
using System.Web.Security;

namespace shop.Areas.Admin.Controllers
{
    public class UsersController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: Admin/Users
        public ActionResult Index(string str)
        {

            var users = db.Users.Include(u => u.Role).ToList();
            if (!string.IsNullOrEmpty(str))
            {
                users = users.Where(u=> u.UserMobile.Contains(str)).ToList();
            }
            return View(users.ToList());
        }
        public ActionResult ShowAddress(int? id)
        {
            var address = db.Addresses.Where(a => a.UserId == id).ToList();
            return View(address);
        }

        // GET: Admin/Users/Create
        public ActionResult Create()
        {
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Roletitle");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,RoleId,UserMobile,UserPassword,UserCode,IsActive")] User user)
        {
            if (ModelState.IsValid)
            {
                if(!db.Users.Any(u=> u.UserMobile == user.UserMobile))
                {
                    Random random = new Random();
                    int mycode = random.Next(100000, 999000);
                    Models.User usr = new User()
                    {
                        IsActive = user.IsActive,
                        RoleId = user.RoleId,
                        UserMobile = user.UserMobile,
                        UserPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(user.UserPassword,"MD5"),
                        UserCode = mycode.ToString()
                    };
                    db.Users.Add(usr);
                    db.SaveChanges();
                }
                else
                {
                    ModelState.AddModelError("UserMobile", "شما نمی توانید با این شماره موبایل کاربری ثبت نمایید");
                }


             
                return RedirectToAction("Index");
            }

            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Roletitle", user.RoleId);
            return View(user);
        }

        // GET: Admin/Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Roletitle", user.RoleId);
            return View(user);
        }

        // POST: Admin/Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,RoleId,UserMobile,UserPassword,UserCode,IsActive")] User user)
        {
            if (ModelState.IsValid)
            {
                var usr = db.Users.FirstOrDefault(u=> u.Id == user.Id);
                if(!db.Users.Any(u=> u.Id != user.Id && u.UserMobile == user.UserMobile))
                {
                    usr.UserPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(user.UserPassword, "MD5");
                    usr.RoleId = user.RoleId;
                    usr.UserMobile = user.UserMobile;
                    usr.IsActive = user.IsActive;
                    db.SaveChanges();
                }
                else
                {
                    ModelState.AddModelError("UserMobile","شماره موبایل تکراری است");
                }


                
                return RedirectToAction("Index");
            }
            ViewBag.RoleId = new SelectList(db.Roles, "Id", "Roletitle", user.RoleId);
            return View(user);
        }

        // GET: Admin/Users/Delete/5
        public ActionResult Delete(int? id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

   

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
