﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using shop.Models;

namespace shop.Areas.Admin.Controllers
{
    public class ProductsController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: Admin/Products
        public ActionResult Index(string strsearch)
        {
           
            var products = db.products.Include(p => p.Brand).Include(p => p.Group).ToList();
            if (!string.IsNullOrEmpty(strsearch)) {

                products = products.Where(p => p.Name.Contains(strsearch) || p.Group.GroupName.Contains(strsearch) || p.Brand.BrandName.Contains(strsearch) ).ToList();

            }
            return View(products.ToList());
        }

        // GET: Admin/Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Admin/Products/Create
        public ActionResult Create()
        {
            ViewBag.BrandId = new SelectList(db.Brands, "Id", "BrandName");
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "GroupName");
            return View();
        }

        // POST: Admin/Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,BrandId,GroupId,Name,price,Img,Des,seen,Mojoodi,NotShow")] Product product, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if(file != null)
                {
                    Random random = new Random();
                    string imgcode = random.Next(10000, 99999).ToString();
                    file.SaveAs(HttpContext.Server.MapPath("~/images/Product/") +imgcode.ToString() + "-" + file.FileName);
                    product.Img = imgcode.ToString() + "-" + file.FileName;
                }
                db.products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BrandId = new SelectList(db.Brands, "Id", "BrandName", product.BrandId);
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "GroupName", product.GroupId);
            return View(product);
        }

        // GET: Admin/Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.BrandId = new SelectList(db.Brands, "Id", "BrandName", product.BrandId);
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "GroupName", product.GroupId);
            return View(product);
        }

        // POST: Admin/Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,BrandId,GroupId,Name,price,Img,Des,seen,Mojoodi,NotShow")] Product product, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    Random random = new Random();
                    string imgcode = random.Next(10000, 99999).ToString();
                    file.SaveAs(HttpContext.Server.MapPath("~/images/Product/") + imgcode.ToString() + "-" + file.FileName);
                    product.Img = imgcode.ToString() + "-" + file.FileName;
                }
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BrandId = new SelectList(db.Brands, "Id", "BrandName", product.BrandId);
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "GroupName", product.GroupId);
            return View(product);
        }

        // GET: Admin/Products/Delete/5
        public ActionResult Delete(int? id)
        {
            Product product = db.products.Find(id);
            db.products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
