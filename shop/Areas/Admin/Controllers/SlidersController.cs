﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using shop.Models;

namespace shop.Areas.Admin.Controllers
{
    public class SlidersController : Controller
    {

        private DatabaseContext db = new DatabaseContext();

        // GET: Admin/Sliders
        public ActionResult Index()
        {
            return View(db.Sliders.ToList());
        }

        // GET: Admin/Sliders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Slider slider = db.Sliders.Find(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }

        // GET: Admin/Sliders/Create
        public ActionResult Create()
        {
            return View();
        }
       
        // POST: Admin/Sliders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,SliderTitle,SliderImage,SlidetLink,SliderNotShow")] Slider slider, HttpPostedFileBase file)
        {
            Random random = new Random();
            string imgcode = random.Next(10000, 99999).ToString();
            if (ModelState.IsValid)
            {
                if(file != null)
                {
                    file.SaveAs(HttpContext.Server.MapPath("~/images/") + imgcode.ToString() + "-" + file.FileName);
                    slider.SliderImage = imgcode.ToString() + "-" + file.FileName;
                }
                db.Sliders.Add(slider);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(slider);
        }

        // GET: Admin/Sliders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Slider slider = db.Sliders.Find(id);
            if (slider == null)
            {
                return HttpNotFound();
            }
            return View(slider);
        }

        // POST: Admin/Sliders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,SliderTitle,SliderImage,SlidetLink,SliderNotShow")] Slider slider, HttpPostedFileBase file)
        {
            Random random = new Random();
            string imgcode = random.Next(10000, 99999).ToString();
            if (ModelState.IsValid)
            {
                if(file != null)
                {
                    file.SaveAs(HttpContext.Server.MapPath("~/images/") + imgcode.ToString() + "-" + file.FileName);
                    slider.SliderImage = imgcode.ToString() + "-" + file.FileName;
                }

                db.Entry(slider).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(slider);
        }

        // GET: Admin/Sliders/Delete/5
        public ActionResult Delete(int? id)
        {
            Slider slider = db.Sliders.Find(id);
            db.Sliders.Remove(slider);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

      
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
