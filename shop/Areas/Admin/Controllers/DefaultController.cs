﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using shop.Models;
namespace shop.Areas.Admin.Controllers
{
    public class DefaultController : Controller
    {
        DatabaseContext db = new DatabaseContext();
        // GET: Admin/Default
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SiteSetting()
        {
            var setting = db.Settings.FirstOrDefault();

            return View(setting);

        }
        [HttpPost]
        public ActionResult SiteSetting(Setting setting)
        {
            var set = db.Settings.FirstOrDefault();
            set.ShopName = setting.ShopName;
            set.ShopTel = setting.ShopTel;
            set.ShopAddress = setting.ShopAddress;
            set.ShopKey = setting.ShopKey;
            set.ShopDes = setting.ShopDes;
            db.SaveChanges();
            return View(setting);

        }

        public ActionResult SmsSetting()
        {
            var setting = db.Settings.FirstOrDefault();
            return View(setting);

        }
        [HttpPost]
        public ActionResult SmsSetting(Setting setting)
        {
            var set = db.Settings.FirstOrDefault();
            set.SmsSender = setting.SmsSender;
            set.SmsApi = setting.SmsApi;
            set.PayIsSend = setting.PayIsSend;
            set.FactorIsSend = setting.FactorIsSend;
            db.SaveChanges();
            return View(setting);

        }
    }
}