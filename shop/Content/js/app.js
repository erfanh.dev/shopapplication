﻿
var swiper = new Swiper('.swiper-container', {
    effect: 'slide',
    loop: true,
    noSwiping: false,
    autoplay: false,


    slidesPerView: 4,
    spaceBetween: 5,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        350: {
            slidesPerView: 1,
            spaceBetween: 5,
            keyboard: true,
        },
        640: {
            slidesPerView: 2,
            spaceBetween: 20,
            keyboard: true,
        },
        768: {
            slidesPerView: 3,
            spaceBetween: 30,
            keyboard: true,
        },
        1024: {
         
            spaceBetween: 10,
            keyboard: false,
        },
    }
});
