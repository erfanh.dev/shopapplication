﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using shop.Models;
using System.Web.Security;
using shop.Classes;
namespace shop.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        DatabaseContext db = new DatabaseContext();
     
        
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
     
        public ActionResult Register(RegisterViewModel register)
        {
            if (ModelState.IsValid)
            {
                if (!db.Users.Any(u => u.UserMobile == register.UserMobile))
                {
                    string myHash = FormsAuthentication.HashPasswordForStoringInConfigFile(register.UserPassword, "MD5");
                    Random random = new Random();
                    int mycode = random.Next(100000, 999000);
                    Models.User user = new User()
                    {
                        RoleId = db.Roles.Max(r => r.Id),
                        UserMobile = register.UserMobile,
                        UserPassword = myHash,
                        UserCode = mycode.ToString()
                    };
                    db.Users.Add(user);
                    db.SaveChanges();

                    SmsSender sms = new SmsSender();
                    
                    sms.Send(register.UserMobile, "ثبت نام شما در فروشگاه انجام شد" + Environment.NewLine + "کدفعالسازی شما :" + mycode.ToString());

                    return RedirectToAction("Login");
                }
                else
                {
                    ModelState.AddModelError("UserMobile", "شما قبلا ثبت نام کرده اید");
                }
            }

            return View(register);
        }


        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel login)
        {
            if (ModelState.IsValid)
            {
                string myhash = FormsAuthentication.HashPasswordForStoringInConfigFile(login.UserPassword, "MD5");

                var user = db.Users.FirstOrDefault(u => u.UserMobile == login.UserMobile && u.UserPassword == myhash);
                if (user != null)
                {
                    FormsAuthentication.SetAuthCookie(login.UserMobile, true);
                    if (user.IsActive)
                    {
                        if (user.Role.Rolename == "admin")
                        {
                            return Redirect("/Admin/Default/Index");
                        }
                        else
                        {
                            return RedirectToAction("Index", "Profile");
                        }
                    }
                    else
                    {
                        return RedirectToAction("Activate");
                    }

                }
                else
                {
                    ModelState.AddModelError("UserMobile", "کاربری با این مشخصات وجود ندارد");
                }
            }
          
            return View();
        }

        public ActionResult Activate()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Activate(ActivateViewModel activate)
        {
            var user = db.Users.FirstOrDefault(u => u.UserMobile == User.Identity.Name && u.UserCode == activate.UserCode);
            if (user != null)
            {
                Random random = new Random();
                int mycode = random.Next(100000, 999000);
                user.IsActive = true;
                user.UserCode = mycode.ToString();
                db.SaveChanges();
                return RedirectToAction("Index", "Profile");
            }
            else
            {
                ModelState.AddModelError("UserCode", "کد فعالسازی شما اشتباه وارد شده است");
            }
            return View(activate);
        }
        public ActionResult CheckMobile()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CheckMobile(CheckMobileViewModel check)
        {
            if (ModelState.IsValid)
            {
                var user = db.Users.FirstOrDefault(u => u.UserMobile == check.UserMobile);
                if(user != null)
                {
                    SmsSender sms = new SmsSender();
                    sms.Send(check.UserMobile, "کد تایید شما برای تغییر کلمه عبور: " + user.UserCode + "می باشد ");
                    return RedirectToAction("ForgetPassword");
                }
                else
                {
                    ModelState.AddModelError("UserMobile", "کاربری با این مشخصات وجود ندارد");
                }
            }
            return View();
        }

        public ActionResult ForgetPassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ForgetPassword(ForgetPasswordViewModel forget)
        {
            if (ModelState.IsValid)
            {
                var user = db.Users.FirstOrDefault(u => u.UserCode == forget.UserCode);
                if(user != null)
                {
                    Random random = new Random();
                    int mycode = random.Next(100000, 999000);
                    user.UserPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(forget.UserPassword, "MD5");
                    user.UserCode = mycode.ToString();
                    db.SaveChanges();
                    return RedirectToAction("Login");
                }
                else
                {
                    ModelState.AddModelError("UserCode", "کد تایید شما معتبر نمی باشد");
                }
            }

            return View(forget);
        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
    }

}