﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using shop.Models;
using System.Web.Security;

namespace shop.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
      private  DatabaseContext db = new DatabaseContext();
        
        // GET: Profile
        public ActionResult Index()
        {
            var user = db.Users.FirstOrDefault(u=> u.UserMobile == User.Identity.Name);
            if (user.IsActive == false)
            {
                return RedirectToAction("Activate", "Account");
            }
            else
            {
                return View();
            }
            
        }
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel change)
        {
            string mayhash = FormsAuthentication.HashPasswordForStoringInConfigFile(change.UserOldPassword, "MD5");
            var user = db.Users.FirstOrDefault(u=> u.UserMobile== User.Identity.Name && u.UserPassword == mayhash);
            if(user != null)
            {
                user.UserPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(change.UserPassword, "MD5");
                db.SaveChanges();
              if(user.Role.Rolename == "admin")
                {
                    return Redirect("/Admin/Default");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                ModelState.AddModelError("UserOldPassword", "رمزعبور فعلی وارد شده صحیح نمی باشد");
            }
            return View(change);
        }
    }
}