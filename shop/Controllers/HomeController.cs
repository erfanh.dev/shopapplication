﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using shop.Models;

namespace shop.Controllers
{
    public class HomeController : Controller
    {
        DatabaseContext db = new DatabaseContext();
       
        public ActionResult Index()
        {
            db.Settings.ToList();
            return View();
        }

        public ActionResult Menu()
        {
            var menu = db.Menus.Where(m => m.MenuNotshow == false).OrderBy(m => m.MenuOrder).ToList();
            return PartialView(menu);
        }

        public ActionResult Showmenu(int? id)
        {
            var menu = db.Menus.Find(id);
            return View(menu);
        }

        public ActionResult Slider()
        {
            var slider = db.Sliders.Where(s=> s.SliderNotShow == false).ToList();
            return PartialView(slider);
        }

        public ActionResult Group()
        {
            var group = db.Groups.Where(g => g.GroupNotShow == false).OrderBy(g => g.GroupOrder).ToList();
            return PartialView(group);
        }
        public ActionResult ShowProduct()
        {
            var product = db.products.Where(p => p.NotShow == false).Take(10).ToList();
            return PartialView(product);
        }
        [Route("product/{id}/{title}")]
        public ActionResult product(int? id, string title)
        {
            var product = db.products.Find(id);
            product.seen += 1;
            db.SaveChanges();
            return View(product);
        }

        public ActionResult gallery(int? id)
        {
            var gallery = db.Galleries.Where(g => g.ProductId == id).ToList();
            return PartialView(gallery);
        }
    }
}