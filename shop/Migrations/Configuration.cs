namespace shop.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.Security;
    using shop.Models;
    internal sealed class Configuration : DbMigrationsConfiguration<shop.Models.DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "shop.Models.DatabaseContext";
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(shop.Models.DatabaseContext context)
        {
            DatabaseContext db = new DatabaseContext();
            string myHash = FormsAuthentication.HashPasswordForStoringInConfigFile("0020", "MD5");
            if (db.Roles.Count() == 0)
            {
                Role role = new Role() { 
                Rolename="admin",
                Roletitle="مدیر"
                };
                db.Roles.Add(role);
                db.SaveChanges();


                User user = new User()
                {
                    IsActive=true,
                    UserCode="666666",
                    RoleId = role.Id,
                    UserMobile="09396803077",
                    UserPassword= myHash,

                };
                db.Users.Add(user);
                db.SaveChanges();


                Role role2 = new Role()
                {
                    Rolename = "user",
                    Roletitle = "کاربر"
                };
                db.Roles.Add(role2);
                db.SaveChanges();


            }

            if (db.Settings.Count() == 0)
            {
                Setting setting = new Setting()
                {
                    FactorIsSend = false,
                    PayIsSend = false,
                    ShopName = "فروشگاه اینترنتی"
                    
                };
            db.Settings.Add(setting);
                db.SaveChanges();
            }
        }
    }
}
